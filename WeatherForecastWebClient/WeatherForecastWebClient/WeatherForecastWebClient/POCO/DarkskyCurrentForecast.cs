﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class DarkskyCurrentForecast
    {
        public DateTime dateTime { get; }
        public double temp { get; }

        public DarkskyCurrentForecast(DateTime dateTime, double temp)
        {
            this.dateTime = dateTime;
            this.temp = temp;
        }
    }
}
