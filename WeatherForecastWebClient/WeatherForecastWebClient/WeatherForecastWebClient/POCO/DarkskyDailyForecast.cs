﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class DarkskyDailyForecast
    {
        public DateTime dateTime { get; }
        public double maxTemp { get; }
        public double minTemp { get; }

        public DarkskyDailyForecast(DateTime dateTime, double maxTemp, double minTemp)
        {
            this.dateTime = dateTime;
            this.maxTemp = maxTemp;
            this.minTemp = minTemp;
        }
    }
}
