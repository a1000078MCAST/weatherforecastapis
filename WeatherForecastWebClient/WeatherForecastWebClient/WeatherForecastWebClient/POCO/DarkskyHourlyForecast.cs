﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class DarkskyHourlyForecast
    {
        public DateTime dateTime { get; }
        public double temp { get; }

        public DarkskyHourlyForecast(DateTime dateTime, double temp)
        {
            this.dateTime = dateTime;
            this.temp = temp;
        }
    }
}
