﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class Forecast
    {
        public DateTime dateTime { get; }
        public float temperature { get; }

        public Forecast(DateTime dateTime, float temperature)
        {
            this.dateTime = dateTime;
            this.temperature = temperature;
        }
    }
}
