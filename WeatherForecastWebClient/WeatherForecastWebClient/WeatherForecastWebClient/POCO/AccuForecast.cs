﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class AccuForecast
    {
        public DateTime dateTime { get; }
        public double maxTemp { get; }
        public double minTemp { get; }

        public AccuForecast(DateTime dateTime, double maxTemp, double minTemp)
        {
            this.dateTime = dateTime;
            this.maxTemp = maxTemp;
            this.minTemp = minTemp;
        }
    }
}
