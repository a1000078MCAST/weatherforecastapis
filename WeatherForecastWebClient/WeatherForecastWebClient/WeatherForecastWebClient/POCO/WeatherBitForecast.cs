﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.POCO
{
    class WeatherBitForecast
    {
        public DateTime dateTime { get; }
        public double temp { get; set; }

        public WeatherBitForecast(DateTime dateTime, double temp)
        {
            this.dateTime = dateTime;
            this.temp = temp;
        }
    }
}
