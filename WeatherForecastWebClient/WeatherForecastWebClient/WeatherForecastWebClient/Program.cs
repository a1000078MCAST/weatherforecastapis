﻿using System;
using System.Text.Json;
using WeatherForecastWebClient.EndPoint;
using WeatherForecastWebClient.WeatherModel;
using WeatherForecastWebClient.ForecastModel;
using WeatherForecastWebClient.Output;
using WeatherForecastWebClient.WebClient;
using WeatherForecastWebClient.Parser;
using WeatherForecastWebClient.Endpoints;
using WeatherForecastWebClient.Controllers;
using WeatherForecastWebClient.POCO;
using WeatherForecastWebClient.Models;

namespace WeatherForecastWebClient
{
    class Program
    {

        static void Main(string[] args)
        {
            //Opneweathermap Weather API
            OpenWeatherMapAPI();

            ProcessOpenWeatherMapForecastModel();

            //Accuweather Weather API
            AccuweatherCurrentConditionsAPI();

            AccuweatherForecastAPI();


            //Weatherbit Weather API
            WeatherBitCurrentWeatherAPI();

            WeatherBit16DayForecast();


            //Darksky Weather API 
            DarkskyCurrentForecast();

            DarkskyDailyForecast();

            DarkskyHourlyForecast();

            Console.WriteLine("*** ALL APIS CALLED ***\nPress Enter to Exit!");
            Console.ReadKey();
        }

        static void OpenWeatherMapAPI()
        {
            Out output = new Out();
            output.outputToConsole("Getting current temperature for Marsa");

            OpenWeatherMapController openWeatherMapController = new OpenWeatherMapController();
            output.outputToConsole($"Temperature: {openWeatherMapController.getCurrentTemperature("Marsa", EndpointTypes.CURRENTWEATHER)}");

        }



        static void ProcessOpenWeatherMapForecastModel()
        {
            Out output = new Out();

            output.outputToConsole(System.Environment.Version.ToString());

            OpenWeatherMapController openWeatherMapController = new OpenWeatherMapController();


            output.outputToConsole("***** Open Weather Map Forecast *****");
            output.outputToConsole("  *** 5 DAY FORECAST FOR MARSA  ***");
            output.outputToConsole("***** Open Weather Map Forecast *****");

            foreach (Forecast forecast in openWeatherMapController.getForecastList("Marsa", EndpointTypes.FORECAST))
            {
                output.outputToConsole($"Date/Time: {forecast.dateTime.ToLongDateString()} {forecast.dateTime.ToLongTimeString()} - Temperature: {forecast.temperature}");
            }

            output.outputToConsole("***** End of Weather Forecast *****");
        }

        static void AccuweatherCurrentConditionsAPI()
        {
            Out output = new Out();

            AccuWeatherController accuWeatherController = new AccuWeatherController();

            output.outputToConsole("***** AccuWeather Current Conditions *****");
            string cityName = "Marsa,MT";

            output.outputToConsole("Location: Marsa,MT");
            output.outputToConsole("Key: " + accuWeatherController.getLocationKey(cityName));
            output.outputToConsole("Current Temperature: " + accuWeatherController.getCurentWeather("Marsa,MT"));
        }

        static void AccuweatherForecastAPI()
        {
            Out output = new Out();

            AccuWeatherController accuWeatherController = new AccuWeatherController();

            output.outputToConsole("***** AccuWeather 5 Day Forecast *****");
            string cityName = "Marsa,MT";

            output.outputToConsole("Location: Marsa,MT");
            output.outputToConsole("Key: " + accuWeatherController.getLocationKey(cityName));
            output.outputToConsole("***FORECAST***");

            foreach (AccuForecast forecast in accuWeatherController.get5DayForecast("Marsa,MT"))
            {
                output.outputToConsole($"Date/Time: {forecast.dateTime.ToLongDateString()} {forecast.dateTime.ToLongTimeString()} - Temp Min/Max: {forecast.minTemp}C/{forecast.maxTemp}C ");
            }
        }

        static void WeatherBitCurrentWeatherAPI()
        {
            Out output = new Out();

            WeatherBitController weatherBitController = new WeatherBitController();

            output.outputToConsole("***** WeatherBit Current Conditions *****");
            string cityName = "Marsa";
            string countryName = "Malta";

            output.outputToConsole($"Location: {cityName}, {countryName}");
            output.outputToConsole("Current Temperature: " + weatherBitController.getCurentWeather("Marsa", "Malta"));
        }

        static void WeatherBit16DayForecast()
        {
            Out output = new Out();

            WeatherBitController weatherBitController = new WeatherBitController();

            output.outputToConsole("***** WeatherBit 5 Day Forecast *****");
            string cityName = "Marsa";
            string countryName = "Malta";

            output.outputToConsole("Location: Marsa,MT");
            output.outputToConsole("***FORECAST***");

            foreach (WeatherBitForecast forecast in weatherBitController.getForecastList(cityName, countryName))
            {
                output.outputToConsole($"Date/Time: {forecast.dateTime.ToLongDateString()} {forecast.dateTime.ToLongTimeString()} - Temp: {forecast.temp} ");
            }
        }

        static void DarkskyCurrentForecast()
        {
            Out output = new Out();
            DarkskyController darkskyController = new DarkskyController();

            string longitude = "35.8736";
            string latitude = "-14.4927";

            output.outputToConsole("***** DarkSky Current Forecast *****");

            DarkskyCurrentForecast darkskyCurrent = darkskyController.getCurrentForecast(longitude, latitude);

            output.outputToConsole("Longitude: " + longitude);
            output.outputToConsole("Latitude: " + latitude);

            output.outputToConsole($"Date/Time: {darkskyCurrent.dateTime.ToLongDateString()} {darkskyCurrent.dateTime.ToLongTimeString()} - Temp: {darkskyCurrent.temp} ");
        }

        static void DarkskyDailyForecast()
        {
            Out output = new Out();

            DarkskyController darkskyController = new DarkskyController();

            string longitude = "35.8736";
            string latitude = "-14.4927";

            output.outputToConsole("***** DarkSky Daily Forecast *****");

            foreach (DarkskyDailyForecast forecast in darkskyController.getDailyForecastList(longitude, latitude))
            {
                output.outputToConsole($"Date/Time: {forecast.dateTime.ToLongDateString()} {forecast.dateTime.ToLongTimeString()} - Min/Max Temp: {forecast.minTemp}/{forecast.maxTemp}");
            }
        }

        static void DarkskyHourlyForecast()
        {
            Out output = new Out();

            DarkskyController darkskyController = new DarkskyController();

            string longitude = "35.8736";
            string latitude = "-14.4927";

            output.outputToConsole("***** DarkSky Hourly Forecast *****");

            foreach (DarkskyHourlyForecast forecast in darkskyController.getHourlyForecastList(longitude, latitude))
            {
                output.outputToConsole($"Date/Time: {forecast.dateTime.ToLongDateString()} {forecast.dateTime.ToLongTimeString()} - Temp: {forecast.temp}");
            }
        }
    }


}
