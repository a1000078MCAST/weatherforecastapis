﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherForecastWebClient.Endpoints;

namespace WeatherForecastWebClient.EndPoint
{
    class OpenWeatherMapEndpoint : Endpoint
    {

        public OpenWeatherMapEndpoint(): base(
            "5204a63da1dd675b5f1334ffd21d59b3", 
            "http://api.openweathermap.org/data/", 
            new Dictionary<EndpointTypes, string> { 
                { EndpointTypes.CURRENTWEATHER, "weather" }, 
                { EndpointTypes.FORECAST, "forecast" } },
            "2.5",
            "metric")
        { 
        }


        public string getByCityNameEndpoint(string cityName, EndpointTypes endpointTypes)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"{version}");
            stringBuilder.Append($"/{endpointTypeDictionary[endpointTypes]}");
            stringBuilder.Append("?q=");
            stringBuilder.Append(cityName);
            stringBuilder.Append("&appid=");
            stringBuilder.Append(apiKey);
            stringBuilder.Append("&units=");
            stringBuilder.Append(units);

            return stringBuilder.ToString();
        }

        public string getByCityNameEndpoint(string cityName, string countryCode, EndpointTypes endpointTypes)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"{version}");
            stringBuilder.Append($"/{endpointTypeDictionary[endpointTypes]}");
            stringBuilder.Append("?q=");
            stringBuilder.Append(cityName);
            stringBuilder.Append(",");
            stringBuilder.Append(countryCode);
            stringBuilder.Append("&appid=");
            stringBuilder.Append(apiKey);
            stringBuilder.Append("&units=");
            stringBuilder.Append(units);

            return stringBuilder.ToString();
        }

        public string getByCityNameEndpoint(string cityName, string state, string countryCode, EndpointTypes endpointTypes)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"{version}");
            stringBuilder.Append($"/{endpointTypeDictionary[endpointTypes]}");
            stringBuilder.Append("?q=");
            stringBuilder.Append(cityName);
            stringBuilder.Append(",");
            stringBuilder.Append(state);
            stringBuilder.Append(",");
            stringBuilder.Append(countryCode);
            stringBuilder.Append("&appid=");
            stringBuilder.Append(apiKey);
            stringBuilder.Append("&units=");
            stringBuilder.Append(units);

            return stringBuilder.ToString();
        }

        //https://api.openweathermap.org/data/2.5/forecast?q=marsa,MT&appid=5204a63da1dd675b5f1334ffd21d59b3&units=metric
    }
}
