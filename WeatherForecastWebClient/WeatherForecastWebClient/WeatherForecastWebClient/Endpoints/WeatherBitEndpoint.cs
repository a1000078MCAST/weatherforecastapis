﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Endpoints
{
    class WeatherBitEndpoint: Endpoint
    {
        //https://api.weatherbit.io/v2.0/current?city=Marsa&country=Malta&units=M&key=4173f72781fb46cc9b8cf77fb99b83bf

        public WeatherBitEndpoint() : base(
            "4173f72781fb46cc9b8cf77fb99b83bf",
            "https://api.weatherbit.io",
            new Dictionary<EndpointTypes, string> {
                { EndpointTypes.CURRENTWEATHER, "current" },
                { EndpointTypes.FORECAST, "forecast/daily" } },
            "v2.0", "M")
        {
        }

        public string getCurrentConditions(string city, string country)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{version}");
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.CURRENTWEATHER]}");
            stringBuilder.Append($"?city={city}");
            stringBuilder.Append($"&country={country}");
            stringBuilder.Append($"&key={apiKey}");
            stringBuilder.Append($"&units={units}");

            return stringBuilder.ToString();
        }

        //https://api.weatherbit.io/v2.0/forecast/daily?city=Marsa&country=Malta&key=4173f72781fb46cc9b8cf77fb99b83bf&units=M

        public string getWeatherForecast(string city, string country)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{version}");
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.FORECAST]}");
            stringBuilder.Append($"?city={city}");
            stringBuilder.Append($"&country={country}");
            stringBuilder.Append($"&key={apiKey}");
            stringBuilder.Append($"&units={units}");

            return stringBuilder.ToString();
        }
    }


}
