﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Endpoints
{
    class DarkskyEndpoint : Endpoint
    {
        //https://api.darksky.net/forecast/885299a548fbffa7f976d567f795c178/35.8736,-14.4927
        public DarkskyEndpoint() : base(
            "885299a548fbffa7f976d567f795c178", 
            "https://api.darksky.net", 
            new Dictionary<EndpointTypes, string> {
            { EndpointTypes.FORECAST, "forecast" } }, "", "auto")
        {
        }


        public string getAllForecastData(string longitude, string latitude)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.FORECAST]}");
            stringBuilder.Append($"/{apiKey}");
            stringBuilder.Append($"/{longitude},{latitude}");
            stringBuilder.Append($"?units={units}");
            

            return stringBuilder.ToString();
        }
    }
}
