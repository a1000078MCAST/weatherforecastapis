﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Endpoints
{
    class AccuWeatherEndpoint : Endpoint
    {
        public AccuWeatherEndpoint() : base(
            "NUGDxjbLAdstUZWdjMjOMGrHabOF7NfD ",
            "http://dataservice.accuweather.com",
            new Dictionary<EndpointTypes, string> { 
                { EndpointTypes.CURRENTWEATHER, "currentconditions" }, 
                { EndpointTypes.FORECAST, "forecasts" }, 
                { EndpointTypes.LOCALITY, "locations" } },
            "v1")
        {
        }

        public string getLocationsEndpoint(string cityName)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.LOCALITY]}");
            stringBuilder.Append($"/{version}");
            stringBuilder.Append("/cities");
            stringBuilder.Append("/search");
            stringBuilder.Append("?apikey=");
            stringBuilder.Append(apiKey);
            stringBuilder.Append("&q=");
            stringBuilder.Append(cityName);

            return stringBuilder.ToString();
        }

        public string getCurrentConditions(string locationKey)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.CURRENTWEATHER]}");
            stringBuilder.Append($"/{version}");
            stringBuilder.Append($"/{locationKey}");
            stringBuilder.Append($"?apikey=");
            stringBuilder.Append(apiKey);

            return stringBuilder.ToString();
        }

        public string getForecastEndpoint(string locationKey)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(baseEndpoint);
            stringBuilder.Append($"/{endpointTypeDictionary[EndpointTypes.FORECAST]}");
            stringBuilder.Append($"/{version}");
            stringBuilder.Append($"/daily");
            stringBuilder.Append("/5day");
            stringBuilder.Append($"/{locationKey}");
            stringBuilder.Append($"?apikey={apiKey}");
            stringBuilder.Append($"&metric=true");

            return stringBuilder.ToString();
        }
    }
}
