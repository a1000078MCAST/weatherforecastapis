﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Endpoints
{
    public enum EndpointTypes
    {
        CURRENTWEATHER,
        FORECAST,
        LOCALITY
    }

    abstract class Endpoint
    {
        protected string apiKey;
        protected string baseEndpoint;
        protected string units { get; set; }
        protected string version { get; set; }
        protected Dictionary<EndpointTypes, string> endpointTypeDictionary { get; set; }

        public Endpoint(string apiKey, string baseEndpoint,  Dictionary<EndpointTypes, string> endpointTypeDictionary,  string version = "", string units = "") 
        {
            this.apiKey = apiKey;
            this.baseEndpoint = baseEndpoint;
            this.units = units;
            this.endpointTypeDictionary = endpointTypeDictionary;
            this.version = version;
        }

        /*public string getEndpointType(EndpointTypes endpointTypes)
        {
            switch (endpointTypes)
            {
                case EndpointTypes.CURRENTWEATHER:
                    return  endpointTypeDictionary[EndpointTypes.CURRENTWEATHER];
                case EndpointTypes.FORECAST:
                    return endpointTypeDictionary[EndpointTypes.FORECAST];
                case EndpointTypes.LOCALITY:
                    return endpointTypeDictionary[EndpointTypes.LOCALITY];
                default:
                    return endpointTypeDictionary[EndpointTypes.CURRENTWEATHER];
            }
        }*/
    }
}
