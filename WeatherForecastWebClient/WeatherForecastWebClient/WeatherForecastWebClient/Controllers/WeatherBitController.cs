﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherForecastWebClient.Endpoints;
using WeatherForecastWebClient.Models;
using WeatherForecastWebClient.Parser;
using WeatherForecastWebClient.POCO;

namespace WeatherForecastWebClient.Controllers
{
    class WeatherBitController: Controller
    {
        private WeatherBitEndpoint weatherBitEndpoint;

        public WeatherBitController() : base()
        {
            weatherBitEndpoint = new WeatherBitEndpoint();
        }

        public string getCurentWeather(string city, string country)
        {
            restClient.endpoint = this.weatherBitEndpoint.getCurrentConditions(city, country);
            this.response = restClient.makeRequest();

            JSONParser<WeatherBitCurrentModel> jp = new JSONParser<WeatherBitCurrentModel>();
            WeatherBitCurrentModel weatherBitCurrentModel = jp.parseJSON(this.response);


            return ""+weatherBitCurrentModel.data[0].temp;
        }

        public List<WeatherBitForecast> getForecastList(string city, string country)
        {
            List<WeatherBitForecast> forecastList = new List<WeatherBitForecast>();

            restClient.endpoint = this.weatherBitEndpoint.getWeatherForecast(city, country);
            this.response = restClient.makeRequest();

            JSONParser<WeatherBitForecastModel> jp = new JSONParser<WeatherBitForecastModel>();
            WeatherBitForecastModel forecastModel = jp.parseJSON(this.response);

            foreach (Data forecastMain in forecastModel.data)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(forecastMain.ts).UtcDateTime;
                forecastList.Add(new WeatherBitForecast(dateTime, forecastMain.temp));
            }
            return forecastList;
        }
    }
}
