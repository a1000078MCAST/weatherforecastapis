﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherForecastWebClient.EndPoint;
using WeatherForecastWebClient.Endpoints;
using WeatherForecastWebClient.ForecastModel;
using WeatherForecastWebClient.Parser;
using WeatherForecastWebClient.POCO;
using WeatherForecastWebClient.WeatherModel;

namespace WeatherForecastWebClient.Controllers
{
    class OpenWeatherMapController : Controller
    {
        private OpenWeatherMapEndpoint openWeatherMapEndpoint;

        public OpenWeatherMapController() : base()
        {
            this.openWeatherMapEndpoint = new OpenWeatherMapEndpoint();
        }

        public float getCurrentTemperature(string city, EndpointTypes endpointTypes)
        {
            float temperature = 0f;

            restClient.endpoint = this.openWeatherMapEndpoint.getByCityNameEndpoint("Marsa", EndpointTypes.CURRENTWEATHER);
            
            this.response = restClient.makeRequest();


            JSONParser<OpenWeatherMapWeatherModel> jp = new JSONParser<OpenWeatherMapWeatherModel>();
            OpenWeatherMapWeatherModel weatherModel = jp.parseJSON(this.response);
            temperature = weatherModel.main.temp;

            return temperature;
        }

        public List<Forecast> getForecastList(string city, EndpointTypes endpointTypes) {
            List<Forecast> forecastList = new List<Forecast>();

            restClient.endpoint = this.openWeatherMapEndpoint.getByCityNameEndpoint("Marsa", EndpointTypes.FORECAST);
            this.response = restClient.makeRequest();

            JSONParser<OpenWeatherMapForecastModel> jp = new JSONParser<OpenWeatherMapForecastModel>();
            OpenWeatherMapForecastModel forecastModel = jp.parseJSON(this.response);

            foreach (UnnamedObject forecastMain in forecastModel.list)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(forecastMain.dt).UtcDateTime;
                forecastList.Add(new Forecast(dateTime, forecastMain.main.temp));
            }
            return forecastList;
        }
    }
}
