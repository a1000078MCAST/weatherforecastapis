﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherForecastWebClient.Endpoints;
using WeatherForecastWebClient.Models;
using WeatherForecastWebClient.Parser;
using WeatherForecastWebClient.POCO;

namespace WeatherForecastWebClient.Controllers
{
    class AccuWeatherController : Controller
    {
        private AccuWeatherEndpoint accuWeatherEndpoint;

        public AccuWeatherController(): base()
        {
            accuWeatherEndpoint = new AccuWeatherEndpoint();
        }

        public string getLocationKey(string cityName)
        {
            string locationKey = string.Empty;

            restClient.endpoint = accuWeatherEndpoint.getLocationsEndpoint(cityName);
            response = restClient.makeRequest();

            JSONParser<List<AccuWeatherLocationModel>> jp = new JSONParser<List<AccuWeatherLocationModel>>();
            List<AccuWeatherLocationModel> accuWeatherLocationsModel = jp.parseJSON(this.response);


            locationKey = accuWeatherLocationsModel[0].Key;

            return locationKey;
        }


        public string getCurentWeather(string location)
        {
            restClient.endpoint = accuWeatherEndpoint.getCurrentConditions(getLocationKey(location));
            response = restClient.makeRequest();

            JSONParser<List<AccuWeatherCurrentModel>> jp = new JSONParser<List<AccuWeatherCurrentModel>>();
            List<AccuWeatherCurrentModel> accuWeatherCurrentModel = jp.parseJSON(this.response);


            return accuWeatherCurrentModel[0].Temperature.Metric.Value + accuWeatherCurrentModel[0].Temperature.Metric.Unit;
        }


        public List<AccuForecast> get5DayForecast(string location)
        {
            restClient.endpoint = accuWeatherEndpoint.getForecastEndpoint(getLocationKey(location));
            response = restClient.makeRequest();

            List<AccuForecast> forecastList = new List<AccuForecast>();

            JSONParser<AccuWeatherForecastModel> jp = new JSONParser<AccuWeatherForecastModel>();
            AccuWeatherForecastModel forecastModel = jp.parseJSON(this.response);

            foreach (DailyForecast forecastMain in forecastModel.DailyForecasts)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(forecastMain.EpochDate).UtcDateTime;
                forecastList.Add(new AccuForecast(dateTime, forecastMain.Temperature.Maximum.Value, forecastMain.Temperature.Minimum.Value));
            }

            return forecastList;
        }
    }
}
