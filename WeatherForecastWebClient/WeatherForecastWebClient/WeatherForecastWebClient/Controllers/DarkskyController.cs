﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherForecastWebClient.Endpoints;
using WeatherForecastWebClient.Models;
using WeatherForecastWebClient.Parser;
using WeatherForecastWebClient.POCO;

namespace WeatherForecastWebClient.Controllers
{
    class DarkskyController : Controller
    {
        private DarkskyEndpoint darkskyEndpoint;

        public DarkskyController() : base()
        {
            this.darkskyEndpoint = new DarkskyEndpoint();
        }

        public List<DarkskyDailyForecast> getDailyForecastList(string longitude, string latitude)
        {
            List<DarkskyDailyForecast> forecastList = new List<DarkskyDailyForecast>();

            restClient.endpoint = this.darkskyEndpoint.getAllForecastData(longitude, latitude);
            this.response = restClient.makeRequest();

            JSONParser<DarkskyForecastModel> jp = new JSONParser<DarkskyForecastModel>();
            DarkskyForecastModel forecastModel = jp.parseJSON(this.response);


            foreach (DailyData dailyData in forecastModel.daily.data)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(dailyData.time).UtcDateTime;
                forecastList.Add(new DarkskyDailyForecast(dateTime, dailyData.temperatureHigh, dailyData.temperatureMin));
            }

            return forecastList;
        }

        public List<DarkskyHourlyForecast> getHourlyForecastList(string longitude, string latitude)
        {
            List<DarkskyHourlyForecast> forecastList = new List<DarkskyHourlyForecast>();

            restClient.endpoint = this.darkskyEndpoint.getAllForecastData(longitude, latitude);
            this.response = restClient.makeRequest();

            JSONParser<DarkskyForecastModel> jp = new JSONParser<DarkskyForecastModel>();
            DarkskyForecastModel forecastModel = jp.parseJSON(this.response);


            foreach (HourlyData hourlyData in forecastModel.hourly.data)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(hourlyData.time).UtcDateTime;
                forecastList.Add(new DarkskyHourlyForecast(dateTime, hourlyData.temperature));
            }

            return forecastList;
        }

        public DarkskyCurrentForecast getCurrentForecast(string longitude, string latitude)
        {
            restClient.endpoint = this.darkskyEndpoint.getAllForecastData(longitude, latitude);
            this.response = restClient.makeRequest();

            JSONParser<DarkskyCurrentForecastModel> jp = new JSONParser<DarkskyCurrentForecastModel>();
            DarkskyCurrentForecastModel forecastModel = jp.parseJSON(this.response);

            DarkskyCurrentForecast currentForecast = new DarkskyCurrentForecast(DateTimeOffset.FromUnixTimeSeconds(forecastModel.currently.time).UtcDateTime, forecastModel.currently.temperature);
            return currentForecast;
        }
    }
}
