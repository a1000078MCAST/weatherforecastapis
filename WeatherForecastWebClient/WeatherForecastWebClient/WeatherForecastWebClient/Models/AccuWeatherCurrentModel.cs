﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Models
{

    public class Metric
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public int UnitType { get; set; }
    }


    public class Temperature
    {
        public Metric Metric { get; set; }
    }


    /// <summary>
    /// This class is a model for the json data recieved from the AccuWeather Forecast endpoint. Only the variables which I will be using were used.
    /// </summary>
    public class AccuWeatherCurrentModel
    {
        public int EpochTime { get; set; }
        public Temperature Temperature { get; set; }
    }

}
