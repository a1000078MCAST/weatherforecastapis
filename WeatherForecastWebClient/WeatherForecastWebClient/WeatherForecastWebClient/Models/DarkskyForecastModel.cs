﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Models
{
    class DarkskyForecastModel
    {
        public Hourly hourly { get; set; }
        public Daily daily { get; set; }
    }

    public class HourlyData
    {
        public int time { get; set; }
        public double temperature { get; set; }
    }

    public class Hourly
    {
        public List<HourlyData> data { get; set; }
    }

    public class DailyData
    {
        public int time { get; set; }
        public double temperatureHigh { get; set; }
        public double temperatureMin { get; set; }
    }

    public class Daily
    {
        public List<DailyData> data { get; set; }
    }
}
