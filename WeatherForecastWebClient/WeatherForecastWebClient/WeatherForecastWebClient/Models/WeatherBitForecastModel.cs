﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Models
{

    public class Data
    {
        public int ts { get; set; }
        public double temp { get; set; }
    }

    /// <summary>
    /// This class is a model for the json data recieved from WeatherBit Forecast endpoint. Only the variables which I will be suing were used as can bee
    /// seen in the Data class
    /// </summary>
    public class WeatherBitForecastModel
    {
        public List<Data> data { get; set; }
    }
}
