﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.WeatherModel
{
    /// <summary>
    /// This class is a model for the json data recieved from OpenWeatherMap Current Weather endpoint. Only the variables which I will be using were used as can bee
    /// seen in the Main class
    /// </summary>
    class OpenWeatherMapWeatherModel
    {
        public Main main { get; set; }
    }


    
    class Main
    {
        public float temp { get; set; }
        //public float feels_like { get; set; }
        //public float temp_min { get; set; }
        //public float temp_max { get; set; }
        //public float pressure { get; set; }
        //public float humidity { get; set; }
    }
}
