﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace WeatherForecastWebClient.Models
{

    /// <summary>
    /// This class is a model for the json data recieved from Locations from AccuWeather endpoint. Only the variables which I will be using were used as can bee
    /// seen in in this case just the Key for the lcoation
    /// </summary>
    class AccuWeatherLocationModel
    {
        public string Key { get; set; }
    }
}
