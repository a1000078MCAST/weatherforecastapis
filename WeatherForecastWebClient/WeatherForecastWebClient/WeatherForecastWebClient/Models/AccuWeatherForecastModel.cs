﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Endpoints
{

    public class Minimum
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public int UnitType { get; set; }
    }

    public class Maximum
    {
        public double Value { get; set; }
        public string Unit { get; set; }
        public int UnitType { get; set; }
    }

    public class Temperature
    {
        public Minimum Minimum { get; set; }
        public Maximum Maximum { get; set; }
    }

    public class DailyForecast
    {
        public DateTime Date { get; set; }
        public int EpochDate { get; set; }
        public Temperature Temperature { get; set; }
    }


    /// <summary>
    /// This class is a model for the json data recieved from the AccuWeather forecast endpoint. Only the variables which I will be using were used as can bee
    /// seen in the classes
    /// </summary>
    public class AccuWeatherForecastModel
    {
        public List<DailyForecast> DailyForecasts { get; set; }
    }
}
