﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Models
{
    class DarkskyCurrentForecastModel
    {
        public Currently currently { get; set; }
    }
    public class Currently
    {
        public int time { get; set; }
        public double temperature { get; set; }
    }


}
