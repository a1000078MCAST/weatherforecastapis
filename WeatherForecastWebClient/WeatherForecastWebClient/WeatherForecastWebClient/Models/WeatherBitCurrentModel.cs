﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.Models
{
    public class Datum
    {
        public double temp { get; set; }

    }

    /// <summary>
    /// This class is a model for the json data recieved from WeatherBit Current Weather endpoint. Only the variables which I will be using were used as can bee
    /// seen in the Datum class. The only reason for the name is that another model was using data and both classes are in the same name space.
    /// </summary>
    public class WeatherBitCurrentModel
    {
        public List<Datum> data { get; set; }
    }
}
