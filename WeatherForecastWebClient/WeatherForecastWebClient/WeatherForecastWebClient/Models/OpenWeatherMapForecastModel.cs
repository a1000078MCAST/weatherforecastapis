﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherForecastWebClient.ForecastModel
{
    /// <summary>
    /// This class is a model for the json data recieved from OpenWeatherMap Forecast Weather endpoint. Only the variables which I will be using were used as can bee
    /// seen in the Main and Unnamed Object classes
    /// </summary>
    class OpenWeatherMapForecastModel
    {
        public List<UnnamedObject> list { get; set; }
    }

    class UnnamedObject
    {
        public Main main { get; set; }
        public long dt { get; set; }
    }

    class Main
    {
        public float temp { get; set; }
        public float feels_like { get; set; }
        public float temp_min { get; set; }
        public float temp_max { get; set; }
        public float pressure { get; set; }
        public float sea_level { get; set; }
        public float grnd_level { get; set; }
        public float humidity { get; set; }
        public float temp_kf { get; set; }
    }
}
