﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace WeatherForecastWebClient.Parser
{
    class JSONParser<T>
    {
        public T parseJSON(string json)
        {
            var jsonModel = JsonSerializer.Deserialize<T>(json);

            return jsonModel;
        }
    }
}
